import pandas as pd

data = {
    'Product_ID': [101, 102, 103, 104, 105, 106, 107, 201, 202, 203, 204, 301, 302, 303, 401, 402, 403, 501, 502, 503, 504, 601, 602, 603, 701, 702, 703],
    'Product_Name': ['Widget_A', 'Widget_B', 'Widget_C', 'Widget_D', 'Gadget_X', 'Gadget_Y', 'Gadget_Z', 'Shirt_A', 'Shirt_B', 'Pants_A', 'Pants_B', 'Book_A', 'Book_B', 'Book_C', 'Tool_A', 'Tool_B', 'Tool_C', 'Food_A', 'Food_B', 'Drink_A', 'Drink_B', 'Toy_A', 'Toy_B', 'Toy_C', 'Home_A', 'Home_B', 'Home_C'],
    'Category': ['Electronics', 'Electronics', 'Electronics', 'Electronics', 'Electronics', 'Electronics', 'Electronics', 'Apparel', 'Apparel', 'Apparel', 'Apparel', 'Books', 'Books', 'Books', 'Hardware', 'Hardware', 'Hardware', 'Grocery', 'Grocery', 'Grocery', 'Grocery', 'Toys', 'Toys', 'Toys', 'Home', 'Home', 'Home'],
    'Price': [25.99, 35.99, 45.99, 55.99, 49.99, 39.99, 59.99, 19.99, 24.99, 29.99, 34.99, 12.99, 15.99, 9.99, 49.99, 64.99, 74.99, 3.99, 2.99, 1.99, 4.99, 19.99, 29.99, 9.99, 59.99, 79.99, 69.99],
    'Units_Sold': [100, 75, 50, 120, 60, 80, 40, 150, 200, 100, 80, 300, 250, 400, 75, 50, 40, 500, 800, 1000, 400, 150, 120, 300, 50, 30, 40],
    'Revenue': [2599.00, 2699.25, 2299.50, 6718.80, 2999.40, 3199.20, 2399.60, 2998.50, 4998.00, 2999.00, 2799.20, 3897.00, 3997.50, 3996.00, 3749.25, 3249.50, 2999.60, 1995.00, 2392.00, 1990.00, 1996.00, 2998.50, 3598.80, 2997.00, 2999.50, 2399.70, 2799.60],
    'Profit': [500.00, 600.00, 400.00, 850.00, 400.00, 600.00, 300.00, 600.00, 800.00, 400.00, 350.00, 900.00, 1000.00, 1200.00, 600.00, 500.00, 400.00, 400.00, 400.00, 600.00, 300.00, 500.00, 800.00, 400.00, 600.00, 500.00, 400.00]
}

df = pd.DataFrame(data)

print(df)
